package com.sms.onlinesms.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.sms.onlinesms.model.Customer;
import com.sms.onlinesms.model.Group;

import java.util.ArrayList;

/**
 * Created by PRASHANT S on 16/04/2016.
 */
public class OnlineSmsDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "onlinesms.db";

    public OnlineSmsDB(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL("create table customer (customer_uuid text primary key,customer_name text,customer_mobile integer,customer_address text)");
            System.out.println("customer Table created...!!!");

            db.execSQL("create table customer_group (group_uuid text primary key,group_name text)");
            System.out.println("group Table created...!!!");

            db.execSQL("create table group_member (group_member_uuid text primary key,customer_uuid text,group_uuid text, FOREIGN KEY(customer_uuid) REFERENCES customer  (customer_uuid),FOREIGN KEY(group_uuid) REFERENCES customer_group (group_uuid))");
            System.out.println("group_member Table created...!!!");

            db.execSQL("create table customer_message (message_uuid text primary key,message_desc text,message_creation_date text,message_receiver text)");
            System.out.println("message Table created...!!!");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS customer");
        db.execSQL("DROP TABLE IF EXISTS group");
        db.execSQL("DROP TABLE IF EXISTS group_member");
        db.execSQL("DROP TABLE IF EXISTS message");
    }

    /*********************************
     * Database manipulation
     **********************************************/


    /**
     * Contact CRUD customer
     *
     * @param customer
     * @return
     */
    public boolean createContact(Customer customer) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues content = new ContentValues();
            content.put(DB.CUSTOMER.UUID, customer.getCustomerUUID());
            content.put(DB.CUSTOMER.NAME, customer.getCustomerName());
            content.put(DB.CUSTOMER.MOBILE, customer.getCustomerMobile());
            content.put(DB.CUSTOMER.ADDRESS, customer.getCustomerAddress());
            db.insert(DB.TABLE.CUSTOMER, null, content);
            System.out.println(" @@@@@@@@@@@@@@@@ Customer Table Inserted \n" + customer.toString());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static final String whereCustomerID = "customer_uuid = ?";

    public boolean updateContact(Customer customer) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues content = new ContentValues();
            content.put(DB.CUSTOMER.NAME, customer.getCustomerName());
            content.put(DB.CUSTOMER.MOBILE, customer.getCustomerMobile());
            content.put(DB.CUSTOMER.ADDRESS, customer.getCustomerAddress());
            db.update(DB.TABLE.CUSTOMER, content, whereCustomerID, new String[]{customer.getCustomerUUID()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public int deleteContact(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int ret = db.delete(DB.TABLE.CUSTOMER, whereCustomerID, new String[]{id});
        return ret;
    }

    public ArrayList<Customer> getAllContacts() {
        ArrayList<Customer> contactList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor res = db.rawQuery("SELECT * FROM customer", null);
            System.out.println("&&&&&&&&&& length of the record " + res.getCount());
            res.moveToFirst();
            while (res.isAfterLast() == false) {
                String id = res.getString(res.getColumnIndex(DB.CUSTOMER.UUID));
                String name = res.getString(res.getColumnIndex(DB.CUSTOMER.NAME));
                String address = res.getString(res.getColumnIndex(DB.CUSTOMER.ADDRESS));
                int mobile = res.getInt(res.getColumnIndex(DB.CUSTOMER.MOBILE));
                Customer customer = new Customer(id, name, mobile, address,false);
                contactList.add(customer);
                res.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contactList;
    }

    /**
     * CRUD on Group
     */
    public boolean createGroup(Group group) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues content = new ContentValues();
            content.put(DB.GROUP.UUID, group.getGroupUUID());
            content.put(DB.GROUP.NAME, group.getGroupName());
            db.insert(DB.TABLE.GROUP, null, content);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public static final String whereGroupID = "customer_uuid = ?";

    public boolean updateGroup(Group group) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues content = new ContentValues();
            content.put(DB.GROUP.NAME, group.getGroupName());
            db.update(DB.TABLE.CUSTOMER, content, whereGroupID, new String[]{group.getGroupUUID()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    public int deleteGroup(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int ret = db.delete(DB.TABLE.GROUP, whereGroupID, new String[]{id});
        System.out.println("deleteGroup");
        return ret;
    }

    public ArrayList<Group> getAllGroups() {
        ArrayList<Group> groupList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor res = db.rawQuery("SELECT * FROM " + DB.TABLE.GROUP, null);
            System.out.println("&&&&&&&&&& length of the record GROUP " + res.getCount());
            res.moveToFirst();
            while (res.isAfterLast() == false) {
                String id = res.getString(res.getColumnIndex(DB.GROUP.UUID));
                String name = res.getString(res.getColumnIndex(DB.GROUP.NAME));
                Group group = new Group(id, name);
                groupList.add(group);
                res.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return groupList;
    }

}
