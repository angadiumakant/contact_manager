package com.sms.onlinesms.utility;

import org.springframework.http.ContentCodingType;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.Base64Utils;

import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Created by bhanagandi on 10/9/2015.
 */
public class RestUtils {
    public static HttpHeaders getHeaders(String auth, String token) {
        HttpHeaders headers = new HttpHeaders();
        //headers.setContentEncoding(ContentCodingType.GZIP);
        headers.setContentType(MediaType.APPLICATION_JSON);
        //headers.setAcceptEncoding(ContentCodingType.GZIP);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("token", token);
        if (auth != null) {
            String basic = new String(Base64Utils.encode(auth.getBytes(Charset
                    .forName("UTF-8"))));
            headers.set("Authorization", "Basic " + basic);
        }
        return headers;
    }
}

