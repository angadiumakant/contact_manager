package com.sms.onlinesms.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sms.onlinesms.R;
import com.sms.onlinesms.db.OnlineSmsDB;
import com.sms.onlinesms.model.Group;

import java.util.UUID;

/**
 * Created by $SHREE$ on 4/15/2016.
 */
public class AddGroup extends AppCompatActivity implements View.OnClickListener
{
    private OnlineSmsDB db;
    EditText name;
    Button btnAddGroup;
    String namestr;
    String uuid;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        db = new OnlineSmsDB(getApplicationContext());
        btnAddGroup =(Button) findViewById(R.id.add_group) ;
        db = new OnlineSmsDB(getApplicationContext());
        //Accessing ui EditText
        name= (EditText) findViewById(R.id.group_name);

        btnAddGroup.setOnClickListener(this);

    }
    boolean task;
    @Override
    public void onClick(View v) {

        //Storing in string
        namestr = name.getText().toString().trim();
        uuid = UUID.randomUUID().toString();

        //putting inot Group object
        Group group=new Group(uuid,namestr);


        task = db.createGroup(group);
        if (task) {
            startActivity(new Intent(getApplicationContext(),HomeScreen.class));
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "group did not save...!!! Please try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
