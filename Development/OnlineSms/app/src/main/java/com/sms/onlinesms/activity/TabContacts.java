package com.sms.onlinesms.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sms.onlinesms.R;
import com.sms.onlinesms.db.DB;
import com.sms.onlinesms.db.OnlineSmsDB;
import com.sms.onlinesms.model.Customer;

import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class TabContacts extends Fragment {

    private ImageButton btnAdd;
    private ImageButton btnDelete;
    private Button btnEdit;
    private ListView listView;
    private OnlineSmsDB db;
    public ArrayList<Customer> customers;
    public ArrayList<String> customerNames = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_contacts, container, false);
        listView = (ListView) view.findViewById(R.id.listView);
        btnAdd = (ImageButton) view.findViewById(R.id.contact_add);
        //btnDelete = (ImageButton) view.findViewById(R.id.contact_delete);
        try {
            db = new OnlineSmsDB(getContext());
            customers = db.getAllContacts();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (customers != null && customers.size() != 0) {
            for (int i = 0; i < customers.size(); i++) {
                customerNames.add(customers.get(i).getCustomerName());
            }
        } else {
            customerNames.add("No Contacts");
        }
        MyCustomAdapter adapter = new MyCustomAdapter(customerNames, getContext(), customers);
        listView.setAdapter(adapter);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddContact.class));
            }
        });

        return view;
    }

    public class MyCustomAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<String> list = new ArrayList<String>();
        private Context context;
        public ArrayList<Customer> customerList;

        public MyCustomAdapter(ArrayList<String> list, Context context, ArrayList<Customer> customerList) {
            this.list = list;
            this.context = context;
            this.customerList = customerList;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int pos) {
            return list.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.custom_contacts_layout, null);
            }
            //Handle TextView and display string from your list


            TextView contactName = (TextView) view.findViewById(R.id.contactName);
            LinearLayout contactRow = (LinearLayout) view.findViewById(R.id.contactRow);

            contactName.setText(customerNames.get(position));
            if (customerList != null && customerList.size() != 0) {
                contactRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(), "Selected Name is " + customerNames.get(position), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getContext(), EditContact.class));
                    }
                });
            }
            return view;
        }
    }


}