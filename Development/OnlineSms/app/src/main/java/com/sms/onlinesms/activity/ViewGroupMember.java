package com.sms.onlinesms.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.sms.onlinesms.R;

import static com.sms.onlinesms.utility.ApplicationContextProvider.getContext;

public class ViewGroupMember extends AppCompatActivity implements View.OnClickListener {

    ImageButton btnAddMem;
    Button btnDelete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);

        btnAddMem = (ImageButton) findViewById(R.id.group_mem_add);
        btnDelete = (Button) findViewById(R.id.group_mem_delete);


        btnAddMem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddGroupMember.class));
            }
        });

    }

    @Override
    public void onClick(View v) {

    }
}
