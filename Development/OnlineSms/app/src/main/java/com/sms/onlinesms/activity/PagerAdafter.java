package com.sms.onlinesms.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdafter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdafter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabContacts contact1 = new TabContacts();
                return contact1;
            case 1:
                TabGroups group1 = new TabGroups();
                return group1;
            case 2:
                TabMessages message1 = new TabMessages();
                return message1;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}