package com.sms.onlinesms.utility;

/**
 * Created by PRASHANT S on 18/04/2016.
 */
public class Constant {

    public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
    public static final String CUSTOMER_MOBILE = "CUSTOMER_MOBILE";
    public static final String CUSTOMER_ADDRESS = "CUSTOMER_ADDRESS";
    public static final String CUSTOMER_UUID = "CUSTOMER_UUID";
}
