package com.sms.onlinesms.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;

import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sms.onlinesms.R;
import com.sms.onlinesms.db.OnlineSmsDB;
import com.sms.onlinesms.model.Group;

import java.util.ArrayList;


public class TabGroups extends Fragment
{
  ImageButton btnAddGroup;
    ImageButton btnDeleteGroup;
   // Button btnViewGroup;
    private  ListView listView;
    private OnlineSmsDB db;
    public ArrayList<String> groupNames = new ArrayList<>();
    public ArrayList<Group> groups;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view= inflater.inflate(R.layout.tab_groups, container, false);
        listView = (ListView) view.findViewById(R.id.listView);

        try {
            db = new OnlineSmsDB(getContext());
            groups = db.getAllGroups();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (groups != null && groups.size() != 0) {
            for (int i = 0; i < groups.size(); i++) {
                groupNames.add(groups.get(i).getGroupName());
            }
        } else {
            groupNames.add("No groups");
        }
        MyCustomAdapter adapter = new MyCustomAdapter(groupNames, getContext(), groups);


        listView.setAdapter(adapter);

        btnAddGroup = (ImageButton) view.findViewById(R.id.group_add);
        btnDeleteGroup = (ImageButton) view.findViewById(R.id.group_delete);
      //  btnViewGroup = (Button) view.findViewById(R.id.group_mem_view);

        //Redirect to add group
        btnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddGroup.class));
            }
        });


        //Redirect to delete group
        btnDeleteGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), DeleteGroup.class));
            }
        });





      //   btnViewGroup.setOnClickListener(new View.OnClickListener() {
      //   @Override
      //   public void onClick(View v) {
      //   startActivity(new Intent(getContext(), ViewGroupMember.class));
      //   }
      //  });

        return view;
    }
    public class MyCustomAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<String> list = new ArrayList<String>();
        private Context context;
        public ArrayList<Group> groupList;

        public MyCustomAdapter(ArrayList<String> list, Context context, ArrayList<Group> groupList) {
            this.list = list;
            this.context = context;
            this.groupList = groupList;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int pos) {
            return list.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.custom_groups_layout, null);
            }
            //Handle TextView and display string from your list


            TextView groupName = (TextView) view.findViewById(R.id.groupName);
            LinearLayout groupRow = (LinearLayout) view.findViewById(R.id.groupRow);

            groupName.setText(groupNames.get(position));
            if (groupList != null && groupList.size() != 0) {
                groupRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(), "Selected Name is " + groupNames.get(position), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getContext(), ViewGroupMember.class));
                    }
                });
            }
            return view;
        }
    }

}