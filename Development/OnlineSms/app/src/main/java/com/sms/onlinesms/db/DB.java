package com.sms.onlinesms.db;

/**
 * Created by umakant.angadi on 31/03/2016.
 */
public class DB {

    public class TABLE {
        public static final String CUSTOMER = "customer";
        public static final String GROUP = "customer_group";
        public static final String GROUP_MEMBER = "group_member";
        public static final String MESSAGE = "customer_message";
    }

    public class CUSTOMER {
        public static final String UUID = "customer_uuid";
        public static final String NAME = "customer_name";
        public static final String MOBILE = "customer_mobile";
        public static final String ADDRESS = "customer_address";
    }

    public class GROUP {
        public static final String UUID = "group_uuid";
        public static final String NAME = "group_name";
    }

    public class GROUP_MEMBER {
        public static final String UUID = "group_member_uuid";
        public static final String CUSTOMER_UUID = CUSTOMER.UUID;
        public static final String GROUP_UUID = GROUP.UUID;
    }

    public class MESSAGE {
        public static final String UUID = "message_uuid";
        public static final String MESSAGE_DESC = "message_desc";
        public static final String MESSAGE_CREATION_TIME = "message_creation_date";
        public static final String MESSAGE_RECEIVERS = "message_receiver";
    }

}
