package com.sms.onlinesms.model;

/**
 * Created by PRASHANT S on 01/04/2016.
 */
public class Message {

    private String messageUUID;
    private String messageDesc;
    private String messageCreationTime;
    private String messageReceivers;

    public String getMessageUUID() {
        return messageUUID;
    }

    public void setMessageUUID(String messageUUID) {
        this.messageUUID = messageUUID;
    }

    public String getMessageDesc() {
        return messageDesc;
    }

    public void setMessageDesc(String messageDesc) {
        this.messageDesc = messageDesc;
    }

    public String getMessageCreationTime() {
        return messageCreationTime;
    }

    public void setMessageCreationTime(String messageCreationTime) {
        this.messageCreationTime = messageCreationTime;
    }

    public String getMessageReceivers() {
        return messageReceivers;
    }

    public void setMessageReceivers(String messageReceivers) {
        this.messageReceivers = messageReceivers;
    }


    @Override
    public String toString() {
        return "Message{" +
                "messageUUID='" + messageUUID + '\'' +
                ", messageDesc='" + messageDesc + '\'' +
                ", messageCreationTime='" + messageCreationTime + '\'' +
                ", messageReceivers='" + messageReceivers + '\'' +
                '}';
    }
}
