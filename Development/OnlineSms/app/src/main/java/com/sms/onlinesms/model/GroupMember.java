package com.sms.onlinesms.model;

/**
 * Created by umakant.angadi on 01/04/2016.
 */
public class GroupMember {

    private String groupMemberUUID;
    private String customerUUID;
    private String groupUUID;

    public String getGroupMemberUUID() {
        return groupMemberUUID;
    }

    public void setGroupMemberUUID(String groupMemberUUID) {
        this.groupMemberUUID = groupMemberUUID;
    }

    public String getCustomerUUID() {
        return customerUUID;
    }

    public void setCustomerUUID(String customerUUID) {
        this.customerUUID = customerUUID;
    }

    public String getGroupUUID() {
        return groupUUID;
    }

    public void setGroupUUID(String groupUUID) {
        this.groupUUID = groupUUID;
    }

    @Override
    public String toString() {
        return "GroupMember{" +
                "groupMemberUUID='" + groupMemberUUID + '\'' +
                ", customerUUID='" + customerUUID + '\'' +
                ", groupUUID='" + groupUUID + '\'' +
                '}';
    }
}
