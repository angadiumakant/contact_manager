package com.sms.onlinesms.activity;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import com.sms.onlinesms.R;
import com.sms.onlinesms.db.OnlineSmsDB;
import com.sms.onlinesms.model.Customer;

import static com.sms.onlinesms.utility.ApplicationContextProvider.getContext;

/**
 * Created by $SHREE$ on 4/18/2016.
 */
public class AddGroupMember extends AppCompatActivity  {
    private ImageButton btnAdd;
    private Button btnEdit;
    private ListView listView;
    private OnlineSmsDB db;
    public ArrayList<Customer> customers;
    public ArrayList<String> customerNames = new ArrayList<>();

    MyCustomAdapter dataAdapter = null;
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_add_group_mem);
        listView = (ListView) findViewById(R.id.listView);

        try {
            db = new OnlineSmsDB(getContext());
            customers = db.getAllContacts();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (customers != null && customers.size() != 0) {
            for (int i = 0; i < customers.size(); i++) {
                customerNames.add(customers.get(i).getCustomerName());
            }
        } else {
            customerNames.add("No Contacts");
        }
        MyCustomAdapter adapter = new MyCustomAdapter(customerNames, getContext(), customers);
        listView.setAdapter(adapter);



    }

    public class MyCustomAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<String> list = new ArrayList<String>();
        private Context context;
        public ArrayList<Customer> customerList;

        public MyCustomAdapter(ArrayList<String> list, Context context, ArrayList<Customer> customerList) {
            this.list = list;
            this.context = context;
            this.customerList = customerList;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int pos) {
            return list.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.custom_add_group_mem, null);
            }
            //Handle TextView and display string from your list


            TextView contactName = (TextView) view.findViewById(R.id.contactName);
            LinearLayout contactRow = (LinearLayout) view.findViewById(R.id.contactRow);

            contactName.setText(customerNames.get(position));
            if (customerList != null && customerList.size() != 0) {
                contactRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(), "Selected Name is " + customerNames.get(position), Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return view;
        }
    }

}
