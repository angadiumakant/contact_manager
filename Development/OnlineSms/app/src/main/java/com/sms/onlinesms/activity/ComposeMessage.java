package com.sms.onlinesms.activity;

import android.os.AsyncTask;
import android.support.annotation.BoolRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sms.onlinesms.R;
import com.sms.onlinesms.utility.RestUtils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class ComposeMessage extends AppCompatActivity implements View.OnClickListener {

    EditText etReceiver;
    EditText etMessgaeBody;
    Button btnSend;
    String receipents;
    String messageBody;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_message);

        etReceiver = (EditText) findViewById(R.id.receiverPhone);
        etMessgaeBody = (EditText) findViewById(R.id.messageBody);
        btnSend = (Button) findViewById(R.id.btnSend);

        btnSend.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        receipents = etReceiver.getText().toString();
        messageBody = etMessgaeBody.getText().toString();

        sendSMS();
    }

    SendMessageAsync sendMessageAsync = null;
    private void sendSMS() {
        sendMessageAsync = new SendMessageAsync();
        sendMessageAsync.execute((Void)null);
    }

    ResponseEntity<String> entity;
    public class SendMessageAsync extends AsyncTask<Void,Void,Boolean>{

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                RestTemplate restTemplate = new RestTemplate();
                String url = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?user=umakantma@gmail.com:angadiharuge&senderID=TEST SMS&receipientno=8951468960&dcs=0&msgtxt=This is Test message&state=4";
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpEntity<Object> httpEntity = new HttpEntity<Object>( RestUtils.getHeaders(null, ""));
                entity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
            }catch(Exception e){
                e.printStackTrace();
            }
            return true;
        }
    }

}
