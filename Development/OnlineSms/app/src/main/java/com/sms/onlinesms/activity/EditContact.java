package com.sms.onlinesms.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sms.onlinesms.R;
import com.sms.onlinesms.db.OnlineSmsDB;
import com.sms.onlinesms.model.Customer;
import com.sms.onlinesms.utility.Constant;

import java.util.UUID;

public class EditContact extends AppCompatActivity implements View.OnClickListener {
    Button btnSubmit;
    EditText etName;
    EditText etMobile;
    EditText etAddress;
    private OnlineSmsDB db;
    String name, address, uuid, mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        db = new OnlineSmsDB(getApplicationContext());

        etName = (EditText) findViewById(R.id.customer_name);
        etMobile = (EditText) findViewById(R.id.customer_mob_num);
        etAddress = (EditText) findViewById(R.id.customer_address);
        btnSubmit = (Button) findViewById(R.id.customer_save);

        System.out.println("getIntent().getStringExtra(Constant.CUSTOMER_NAME) " + getIntent().getStringExtra(Constant.CUSTOMER_NAME));
        System.out.println("getIntent().getStringExtra(Constant.CUSTOMER_MOBILE) " + getIntent().getLongExtra(Constant.CUSTOMER_MOBILE,0L));
        System.out.println("getIntent().getStringExtra(Constant.CUSTOMER_ADDRESS) " + getIntent().getStringExtra(Constant.CUSTOMER_ADDRESS));
        System.out.println("getIntent().getStringExtra(Constant.CUSTOMER_UUID) " + getIntent().getStringExtra(Constant.CUSTOMER_UUID));

        uuid = getIntent().getStringExtra(Constant.CUSTOMER_UUID);
        etName.setText(getIntent().getStringExtra(Constant.CUSTOMER_NAME));
        etMobile.setText(String.valueOf(getIntent().getLongExtra(Constant.CUSTOMER_MOBILE,0L)));
        etAddress.setText(getIntent().getStringExtra(Constant.CUSTOMER_ADDRESS));


        btnSubmit.setOnClickListener(this);
    }

    boolean task;

    @Override
    public void onClick(View v) {
        name = etName.getText().toString().trim();
        mobile = etMobile.getText().toString().trim();
        address = etAddress.getText().toString();

        Customer customer = new Customer(uuid, name, Long.valueOf(mobile), address,false);

        task = db.updateContact(customer);
        if (task) {
            startActivity(new Intent(getApplicationContext(), HomeScreen.class));
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Contact did not update...!!! Please try again.", Toast.LENGTH_SHORT).show();
        }


    }
}
