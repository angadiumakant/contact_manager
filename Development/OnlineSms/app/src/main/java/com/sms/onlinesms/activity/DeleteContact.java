package com.sms.onlinesms.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sms.onlinesms.R;
import com.sms.onlinesms.db.OnlineSmsDB;
import com.sms.onlinesms.model.Customer;
import com.sms.onlinesms.utility.Constant;

import java.util.ArrayList;

import static com.sms.onlinesms.utility.ApplicationContextProvider.getContext;

/**
 * Created by $SHREE$ on 4/15/2016.
 */
public class DeleteContact extends AppCompatActivity implements View.OnClickListener
{

    private ImageButton btnAdd;
    private ImageButton btnDelete;
    private Button btnEdit;
    private ListView listView;
    private OnlineSmsDB db;
    public ArrayList<Customer> customers;
    public ArrayList<String> customerNames = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_contact);

        listView = (ListView) findViewById(R.id.listView);
        btnAdd = (ImageButton) findViewById(R.id.contact_add);
        btnDelete = (ImageButton) findViewById(R.id.contact_delete);
        try {
            db = new OnlineSmsDB(getContext());
            customers = db.getAllContacts();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (customers != null && customers.size() != 0) {
            for (int i = 0; i < customers.size(); i++) {
                customerNames.add(customers.get(i).getCustomerName());
            }
        } else {
            customerNames.add("No Contacts");
        }
        final MyCustomAdapter adapter = new MyCustomAdapter(customerNames, getContext(), customers);
        listView.setAdapter(adapter);




    }

    @Override
    public void onClick(View v) {

    }

    public class MyCustomAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<String> list = new ArrayList<String>();
        private Context context;
        public ArrayList<Customer> customerList;

        public MyCustomAdapter(ArrayList<String> list, Context context, ArrayList<Customer> customerList) {
            this.list = list;
            this.context = context;
            this.customerList = customerList;
        }
        private class ViewHolder {
            TextView txtContactName;
            CheckBox txtCheck;
            LinearLayout contactRow;
        }
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int pos) {
            return list.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
            //just return 0 if your list items do not have an Id variable.
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
              return convertView;
        }
    }

}
