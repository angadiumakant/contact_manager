package com.sms.onlinesms.model;

/**
 * Created by umakant.angadi on 01/04/2016.
 */
public class Customer {
    private String customerUUID;
    private String customerName;
    private long customerMobile;
    private String customerAddress;
    private boolean selected = false;

    public Customer(String customerUUID, String customerName, long customerMobile, String customerAddress, boolean selected) {
        this.customerUUID = customerUUID;
        this.customerName = customerName;
        this.customerMobile = customerMobile;
        this.customerAddress = customerAddress;
        this.selected = selected;
    }

    public String getCustomerUUID() {
        return customerUUID;
    }

    public void setCustomerUUID(String customerUUID) {
        this.customerUUID = customerUUID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(long customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerUUID='" + customerUUID + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerMobile=" + customerMobile +
                ", customerAddress='" + customerAddress + '\'' +
                ", selected=" + selected +
                '}';
    }
}
