package com.sms.onlinesms.model;

/**
 * Created by umakant.angadi on 01/04/2016.
 */
public class Group {
    private String groupUUID;
    private String groupName;

    public Group(String groupUUID, String groupName) {
        this.groupUUID = groupUUID;
        this.groupName = groupName;
    }

    public String getGroupUUID() {
        return groupUUID;
    }

    public void setGroupUUID(String groupUUID) {
        this.groupUUID = groupUUID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupUUID='" + groupUUID + '\'' +
                ", groupName='" + groupName + '\'' +
                '}';
    }
}
