package com.sms.onlinesms.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sms.onlinesms.*;
import com.sms.onlinesms.db.OnlineSmsDB;
import com.sms.onlinesms.model.Customer;

import java.util.UUID;

public class AddContact extends AppCompatActivity implements View.OnClickListener {
    Button submitbtn;
    EditText name;
    EditText mobnum;
    EditText address;
    private OnlineSmsDB db;
    String namestr, addstr, uuid, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        db = new OnlineSmsDB(getApplicationContext());
        // db.getAllContacts();
        //access EditText from UI
        name = (EditText) findViewById(R.id.customer_name);
        mobnum = (EditText) findViewById(R.id.customer_mob_num);
        address = (EditText) findViewById(R.id.customer_address);
        submitbtn = (Button) findViewById(R.id.customer_save);
        submitbtn.setOnClickListener(this);

    }

    boolean task;

    @Override
    public void onClick(View v) {

        //Storing in string
        namestr = name.getText().toString().trim();
        phone = mobnum.getText().toString().trim();
        uuid = UUID.randomUUID().toString();
        addstr = address.getText().toString();

        Customer customer = new Customer(uuid, namestr, Long.valueOf(phone), addstr,false);

        task = db.createContact(customer);
        if (task) {
            startActivity(new Intent(getApplicationContext(),HomeScreen.class));
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Contact did not save...!!! Please try again.", Toast.LENGTH_SHORT).show();
        }

    }
}
